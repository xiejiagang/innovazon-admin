import request from '@/utils/request'



// 获得会员用户
export function getMemberList(params) {
  return request({
    url: '/member/user/page',
    method: 'get',
    params
  })
}
// 获得会员用户详情d
export function getMemberDetail(id) {
  return request({
    url: '/member/user/get?id=' + id,
    method: 'get'
  })
}
// 更新用户详情
export function updateMember(data) {
  return request({
    url: '/member/user/update',
    method: 'post',
    data: data
  })
}
// 更新会员用户算力
export function updateMemberPoint(data) {
  return request({
    url: '/member/user/update-point',
    method: 'post',
    data: data
  })
}
// 更新会员用户等级
export function updateMemberLevel(data) {
  return request({
    url: '/member/user/update-level',
    method: 'post',
    data: data
  })
}
// 获得会员用户余额
export function updateMemberBalance(data) {
  return request({
    url: '/member/user/update-balance',
    method: 'post',
    data: data
  })
}
// 获得会员列表
export function getRefereeUserPage(data) {
  return request({
    url: '/member/user/getRefereeUserPage',
    method: 'post',
    data: data
  })
}