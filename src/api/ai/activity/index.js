import request from '@/utils/request'

// 创建线下活动
export function createActivity(data) {
  return request({
    url: '/ai/activity/create',
    method: 'post',
    data: data
  })
}

// 更新线下活动
export function updateActivity(data) {
  return request({
    url: '/ai/activity/update',
    method: 'post',
    data: data
  })
}

// 删除线下活动
export function deleteActivity(id) {
  return request({
    url: '/ai/activity/delete?id=' + id,
    method: 'post'
  })
}

// 获得线下活动
export function getActivity(id) {
  return request({
    url: '/ai/activity/get?id=' + id,
    method: 'get'
  })
}

// 获得线下活动分页
export function getActivityPage(params) {
  return request({
    url: '/ai/activity/page',
    method: 'get',
    params
  })
}
// 导出线下活动 Excel
export function exportActivityExcel(params) {
  return request({
    url: '/ai/activity/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
