import request from '@/utils/request'

// 创建技能学院-委托
export function createEntrust(data) {
  return request({
    url: '/ai/entrust/create',
    method: 'post',
    data: data
  })
}

// 更新技能学院-委托
export function updateEntrust(data) {
  return request({
    url: '/ai/entrust/update',
    method: 'post',
    data: data
  })
}

// 删除技能学院-委托
export function deleteEntrust(id) {
  return request({
    url: '/ai/entrust/delete?id=' + id,
    method: 'post'
  })
}

// 获得技能学院-委托
export function getEntrust(id) {
  return request({
    url: '/ai/entrust/get?id=' + id,
    method: 'get'
  })
}

// 获得技能学院-委托分页
export function getEntrustPage(params) {
  return request({
    url: '/ai/entrust/page',
    method: 'get',
    params
  })
}
// 获得技能学院-发起审核
export function initiateReview(id) {
  return request({
    url: '/ai/entrust/initiateReview?id=' + id,
    method: 'get',
  })
}
// 获得技能学院-审核委托
export function reviewEntrust(params) {
  console.log("params",params);
  return request({
    url: '/ai/entrust/reviewEntrust' ,
    method: 'get',
    params
  })
}
// 导出技能学院-委托 Excel
export function exportEntrustExcel(params) {
  return request({
    url: '/ai/entrust/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
