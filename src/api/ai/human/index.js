import request from '@/utils/request'

// 创建
export function createDigitalHuman(data) {
  return request({
    url: '/ai/digital-human/create',
    method: 'post',
    data: data
  })
}

// 更新
export function updateDigitalHuman(data) {
  return request({
    url: '/ai/digital-human/update',
    method: 'put',
    data: data
  })
}

// 删除
export function deleteDigitalHuman(id) {
  return request({
    url: '/ai/digital-human/delete?id=' + id,
    method: 'delete'
  })
}

// 获得
export function getDigitalHuman(id) {
  return request({
    url: '/ai/digital-human/get?id=' + id,
    method: 'get'
  })
}

// 获得
export function getDigitalHumanPage(params) {
  return request({
    url: '/ai/digital-human/page',
    method: 'get',
    params
  })
}
// 导出æ•°å­—äººç®¡ç† Excel
export function exportDigitalHumanExcel(params) {
  return request({
    url: '/ai/digital-human/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}