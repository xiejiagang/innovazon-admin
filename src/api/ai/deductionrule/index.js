import request from '@/utils/request'

// 创建计费规则
export function createDeductionRule(data) {
  return request({
    url: '/ai/deduction-rule/create',
    method: 'post',
    data: data
  })
}

// 更新计费规则
export function updateDeductionRule(data) {
  return request({
    url: '/ai/deduction-rule/update',
    method: 'post',
    data: data
  })
}

// 删除计费规则
export function deleteDeductionRule(id) {
  return request({
    url: '/ai/deduction-rule/delete?id=' + id,
    method: 'post'
  })
}

// 获得计费规则
export function getDeductionRule(id) {
  return request({
    url: '/ai/deduction-rule/get?id=' + id,
    method: 'get'
  })
}

// 获得计费规则分页
export function getDeductionRulePage(params) {
  return request({
    url: '/ai/deduction-rule/page',
    method: 'get',
    params
  })
}
// 导出计费规则 Excel
export function exportDeductionRuleExcel(params) {
  return request({
    url: '/ai/deduction-rule/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
