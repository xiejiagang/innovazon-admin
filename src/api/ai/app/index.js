import request from '@/utils/request'

// 创建应用
export function createApp(data) {
  return request({
    url: '/ai/app/create',
    method: 'post',
    data: data
  })
}

// 更新应用
export function updateApp(data) {
  return request({
    url: '/ai/app/update',
    method: 'post',
    data: data
  })
}

// 删除应用
export function deleteApp(id) {
  return request({
    url: '/ai/app/delete?id=' + id,
    method: 'post'
  })
}

// 获得应用
export function getApp(id) {
  return request({
    url: '/ai/app/get?id=' + id,
    method: 'get'
  })
}

// 获得应用分页
export function getAppPage(params) {
  return request({
    url: '/ai/app/page',
    method: 'get',
    params
  })
}
// 导出应用 Excel
export function exportAppExcel(params) {
  return request({
    url: '/ai/app/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
