import request from '@/utils/request'

// 创建用户案例
export function createUserCase(data) {
  return request({
    url: '/ai/user-case/create',
    method: 'post',
    data: data
  })
}

export function publishUserCase(id) {
  return request({
    url: '/ai/user-case/publish?id=' + id,
    method: 'get'
  })
}
export function unpublishUserCase(id) {
  return request({
    url: '/ai/user-case/unpublish?id=' + id,
    method: 'get'
  })
}
// 更新用户案例
export function updateUserCase(data) {
  return request({
    url: '/ai/user-case/update',
    method: 'post',
    data: data
  })
}

// 删除用户案例
export function deleteUserCase(id) {
  return request({
    url: '/ai/user-case/delete?id=' + id,
    method: 'post'
  })
}

// 获得用户案例
export function getUserCase(id) {
  return request({
    url: '/ai/user-case/get?id=' + id,
    method: 'get'
  })
}

// 获得用户案例分页
export function getUserCasePage(params) {
  return request({
    url: '/ai/user-case/page',
    method: 'get',
    params
  })
}
// 导出用户案例 Excel
export function exportUserCaseExcel(params) {
  return request({
    url: '/ai/user-case/export-excel',
    method: 'get',
    params,
    responseType: 'blob'
  })
}
